function enviar () {
    let tipo_contacto = document.getElementById("tipo-contacto").value;       
    let nombre = document.getElementById("txt-nombre").value;
    let apaterno = document.getElementById("txt-apellido-paterno").value;
    let amaterno = document.getElementById("txt-apellido-materno").value;
    let tipo_servicio = document.getElementById("tipo-servicio").value;
    let descripcion = document.getElementById("descripcion").value;
   
    
    // validaciones 
    let mensaje_error = isEmpty (tipo_contacto, "Tipo de comunicacion");
    mensaje_error = mensaje_error + isEmpty(nombre, "Nombre");
    mensaje_error = mensaje_error + isEmpty(apaterno, "Apellido Paterno");
    mensaje_error = mensaje_error + isEmpty(amaterno, "Apellido Materno");
    mensaje_error = mensaje_error + isEmpty(tipo_servicio, "Tipo de servicio");
    mensaje_error = mensaje_error + isEmpty(descripcion, "Descripcion");

    let msgValidacion = document.getElementById("msg-validacion");
    let msgInformacion = document.getElementById("msg-informacion");
    if (mensaje_error == 0){
        console.log("Datos ingresados de manera correcta");
        msgValidacion.className = "alert alert-succes alert-dismissible fade show";
        msgInformacion.innerHTML = "Datos ingresados de manera correcta"
        clearForm(false);
    }
    else {
        console.log(mensaje_error);
        msgValidacion.className = "alert alert-danger alert-dismissible fade show";
        msgInformacion.innerHTML = mensaje_error.replaceAll("/n","<br>");
        msgValidacion.hidden= false;
    
    }


}


function clearForm(msgvalidacionHidden){
    console.log("INICIO CLEAR FORM");
    clear("tipo-contacto")
    clear("txt-nombre");
    clear("txt-apellido-paterno");
    clear("txt-apellido-materno");
    clear("tipo-servicio");
    clear("txt-descripcion");
    let msgValidacion = document.getElementById("msg-validacion");
    msgValidacion.hidden = msgvalidacionHidden;
    console.log("INICIO CLEAR FORM");
}

